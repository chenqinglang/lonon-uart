﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lononUART
{
    public partial class Form1 : Form
    {
        SerialPort sp = new SerialPort();
        public Displaydelegate disp_delegate;
        public delegate void Displaydelegate(byte[] InputBuf);

        long backup_time = 0;
        /*
         *  获取空间坐标尺寸参数
         *  用于适应窗口拉伸
         */
        int win_width, win_height;
        int box_width, box_height;
        int box_send_width;
        int chack_box_1, chack_box_2;
        int but_3, but_4;

        public Form1()
        {
            InitializeComponent();

            Size newSize = new Size(816, 489);
            this.MinimumSize = this.MinimumSize = newSize;
            this.Size = newSize;

            win_width = this.Width;
            win_height = this.Height;

            box_width = textBox1.Width;
            box_height = textBox1.Height;

            box_send_width = textBox2.Width;

            chack_box_1 = checkBox1.Location.X;
            chack_box_2 = checkBox2.Location.X;
            but_3 = button3.Location.X;
            but_4 = button4.Location.X;

            this.Resize += new System.EventHandler(this.Form_Resize);

            Assembly asm = Assembly.GetExecutingAssembly();
            Stream imgStream = asm.GetManifestResourceStream("lononUART.image.lononBle.png");
            pictureBox1.Image = Image.FromStream(imgStream);

            pictureBox1.SendToBack();

            disp_delegate = new Displaydelegate(DispUI);
            sp.DataReceived += new SerialDataReceivedEventHandler(Comm_DataReceived);

            for (int i = 1; i <= 6; i++)
            {
                string ComName = "COM" + i.ToString();
                try
                {
                    sp.PortName = ComName;
                    if (!sp.IsOpen)
                    {
                        sp.Open();
                        sp.Close();
                    }

                    comboBox1.Items.Add("COM" + i.ToString());
                }
                catch
                {
                    continue;
                }
            }
            comboBox1.SelectedIndex = 0;

            comboBox2.Items.Add("9600");
            comboBox2.Items.Add("57600");
            comboBox2.Items.Add("115200");
            comboBox2.SelectedIndex = 0;
        }

        private void Form_Resize(object sender, System.EventArgs e)
        {
            textBox1.Width = box_width - (win_width - this.Width);
            textBox1.Height = box_height - (win_height - this.Height);

            textBox2.Width = box_send_width - (win_width - this.Width);

            checkBox1.Left = chack_box_1 - (win_width - this.Width);
            checkBox2.Left = chack_box_2 - (win_width - this.Width);

            button3.Left = but_3 - (win_width - this.Width);
            button4.Left = but_4 - (win_width - this.Width);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (sp.IsOpen)
            {
                sp.Close();
                button1.Text = "打开";
                return;
            }

            sp.PortName = comboBox1.Text;
            sp.BaudRate = int.Parse(comboBox2.Text); ;
            sp.DataBits = 8;
            sp.StopBits = (StopBits)1;
            
            try
            {
                if (sp.IsOpen)
                {
                    sp.Close();
                    sp.Open();
                }
                else
                {
                    sp.Open();
                }
                button1.Text = "关闭";
            }
            catch (Exception ex)
            {
                MessageBox.Show("错误：" + ex.Message, "C#串口通信");
            }
        }

        void Comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Byte[] InputBuf = new Byte[128];

            try
            {
                sp.Read(InputBuf, 0, sp.BytesToRead);
                System.Threading.Thread.Sleep(50);
                this.Invoke(disp_delegate, InputBuf);
            }
            catch (TimeoutException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void DispUI(byte[] InputBuf)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            
            if(((DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000) > (backup_time + (50 * 1000)))
                textBox1.Text = textBox1.Text + "\r\n[---]";

            backup_time = (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000;

            textBox1.Text = textBox1.Text + encoding.GetString(InputBuf);
            MessageBox.Show((DateTime.Now.ToUniversalTime().Ticks - 621355968000000000));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (sp.IsOpen)
            {
                try
                {
                    sp.Encoding = System.Text.Encoding.GetEncoding("GB2312");
                    sp.Write(textBox2.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("错误：" + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("请先打开串口！");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
